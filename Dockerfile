FROM python:3.8.5

COPY . .
# WORKDIR /app

# RUN apk add --no-cache gcc musl-dev linux-headers

RUN pip install -r requirements.txt

EXPOSE 8060

CMD ["python", "--version"]
CMD ["python", "app.py"]