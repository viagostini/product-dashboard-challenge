import os
from typing import Any, List, Tuple, Union

import dash
import dash_core_components as dcc
import dash_html_components as html
import flask
import pandas as pd
from dash.dependencies import Input, Output

from src.graph import Graph
from src.repository import ProductRepository
from src.utils.time_series import remove_outliers_from_dataframe

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


repository = ProductRepository("./data")
products = repository.get_all()

app.title = "Product Dashboard"

app.layout = html.Div(
    className="container card app__content bg-white",
    children=[
        html.H1(children="Product Dashboard"),
        html.Div(
            [
                html.Label("Select a Product"),
                dcc.Dropdown(
                    id="product-dropdown",
                    options=[{"label": name, "value": name} for name in products],
                    value=next(iter(products)),
                ),
                html.Label("Select a scenario"),
                dcc.Dropdown(id="scenario-dropdown", multi=True,),
                html.Label("Outlier Threshold (± std. deviations)"),
                dcc.Input(id="outlier", type="number", min=1, value=3,),
                html.P(id="projection-future"),
                html.P(id="average-growth"),
                dcc.Graph(id="line-graph"),
            ],
        ),
        html.A(
            id="download-link",
            className="button",
            children="Download Excel",
            href="/download/download.xlsx",
        ),
    ],
)


@app.callback(
    [Output("scenario-dropdown", "options"), Output("scenario-dropdown", "value")],
    [Input("product-dropdown", "value")],
)
def update_scenarios(product_name: str):
    if not product_name:
        return [], None
    scenarios = repository.get(product_name).scenarios
    options = [{"label": scenario, "value": scenario} for scenario in scenarios]
    return options, options[0]["label"]


@app.callback(
    [
        Output("line-graph", "figure"),
        Output("projection-future", "children"),
        Output("average-growth", "children"),
        Output("projection-future", "style"),
        Output("average-growth", "style"),
    ],
    [
        Input("product-dropdown", "value"),
        Input("scenario-dropdown", "value"),
        Input("outlier", "value"),
    ],
)
def update_graph(
    product_name: str, scenarios: Union[str, List[str]], outlier_threshold: int,
) -> Tuple[Any, ...]:
    if not scenarios:
        return {}, "", "", {}, {}

    if isinstance(scenarios, str):
        scenarios = [scenarios]

    product = repository.get(product_name)
    dataframe = product.concatenate_scenarios(scenarios)
    dataframe = remove_outliers_from_dataframe(
        dataframe, outlier_threshold=outlier_threshold, exclude=["Base Scenario"]
    )

    save_as_excel(dataframe)
    graph = Graph(dataframe)

    figure = graph.figure()
    metrics_content, metrics_style = graph.metrics_content(), graph.metrics_style()

    return (figure, *metrics_content, *metrics_style)


def save_as_excel(dataframe: pd.DataFrame) -> None:
    filename = os.path.join(os.getcwd(), "download", "download.xlsx")
    with pd.ExcelWriter(filename) as writer:
        dataframe.to_excel(writer)


@app.server.route("/download/<path:path>")
def serve_static(path: str):
    root_dir = os.getcwd()
    return flask.send_from_directory(
        os.path.join(root_dir, "download"), path, cache_timeout=0
    )


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8060))
    app.run_server(host="0.0.0.0", port=port, debug=True)
