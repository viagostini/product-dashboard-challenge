from typing import List

import pandas as pd


class DataFrameReader:
    """ Handles the reading of different types of files into a pandas dataframe """

    readers = {"csv": pd.read_csv, "xlsx": pd.read_excel}

    def __init__(self, extension: str) -> None:
        if extension in self.readers:
            self.reader = self.readers[extension]

    def read(self, path: str, names: List[str], index_col: int) -> pd.DataFrame:
        """ Reads a file using the appropriate reader into a pandas dataframe

        :param path: str: path of the file to be read
        :param names: List[str]: list of column names to use
        :param index_col: int: index of the column to be used as index
        :returns: pandas dataframe

        """
        return self.reader(
            path, header=None, parse_dates=[0], index_col=index_col, names=names,
        )
