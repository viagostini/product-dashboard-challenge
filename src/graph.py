from typing import Tuple

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from src.utils.time_series import average_growth, projected_growth
from src.utils.utils import one_year_projection_timestamps


class Graph:
    """ Responsible from generating the graph from given dataframe and its metrics """

    def __init__(self, dataframe: pd.DataFrame) -> None:
        self.dataframe = dataframe
        self.scenarios = list(dataframe.columns)

    def metrics_content(self) -> Tuple[str, str]:
        """ Returns the content of the metrics to be displayed above the graph

        :returns: tuple of str for projected growth and average growth metrics

        """
        if len(self.scenarios) != 1:
            return "", ""

        scenario = self.scenarios[0]

        present, future = one_year_projection_timestamps()
        proj = projected_growth(self.dataframe, scenario, initial=present, end=future)
        avg_growth = average_growth(self.dataframe, scenario, periods=12)

        proj_children = "Projected growth from {}/{} to {}/{}: {:.2f}%".format(
            present.month, present.year, future.month, future.year, proj
        )
        growth_children = f"Average annual growth: {avg_growth:.2f}%"
        return proj_children, growth_children

    def metrics_style(self) -> Tuple[dict, dict]:
        """ If the metrics must be hidden, must use opacity CSS property

        :returns: tuple of dict for projected growth and average growth metrics

        """
        if len(self.scenarios) != 1:
            proj_style = {"opacity": 0}
            growth_style = {"opacity": 0}
        else:
            proj_style = {}
            growth_style = {}
        return proj_style, growth_style

    def figure(self) -> go.Figure:
        """ Generate the figure representing the graph for the current dataframe

        :returns: a plotly Figure object

        """
        figure = px.line(self.dataframe, labels={"variable": "Scenario"})
        figure.update_layout(
            xaxis_title="Date", yaxis_title="Millions R$", hovermode="x"
        )
        figure.update_xaxes(
            rangeslider_visible=True, rangeselector=self.__rangeselector(),
        )
        return figure

    def __rangeselector(self) -> dict:
        return dict(
            buttons=list(
                [
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="YTD", step="year", stepmode="todate"),
                    dict(count=1, label="1y", step="year", stepmode="backward"),
                    dict(count=2, label="2y", step="year", stepmode="backward"),
                    dict(step="all"),
                ]
            )
        )
