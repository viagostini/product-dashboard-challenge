from typing import Tuple

import pandas as pd

from src.dataframe_reader import DataFrameReader
from src.utils.time_series import clean_numeric_column, interpolate_nans


class Scenario:
    """ Wraps a pandas dataframe to conveniently represent a scenario """

    def __init__(self, file_path: str) -> None:
        self.name, self.time_series = self.__read_file(file_path)
        self.range = self.time_series.index

    def __read_file(self, path: str) -> Tuple[str, pd.DataFrame]:
        """ Reads a scenario file, performing some data cleaning steps

        :param path: str: path to the scenario file
        :returns: tuple of scenario's name and its dataframe

        """
        tokens = path.split("/")
        scenario, extension = tokens[-1].split(".")
        scenario = scenario.title().replace("_", " ")
        dataframe = DataFrameReader(extension=extension).read(
            path=path, names=["Date", scenario], index_col=0
        )
        clean_numeric_column(dataframe, scenario)
        dataframe[scenario] = interpolate_nans(dataframe, scenario)
        return scenario, dataframe

    def reindex(self, index: pd.DatetimeIndex) -> None:
        """ Replaces its dataframe index inplace, interpolating any missing values

        :param index: pd.DatetimeIndex: new index to be used

        """
        self.range = index
        self.time_series = self.time_series.reindex(index=index)
        self.time_series[self.name] = interpolate_nans(self.time_series, self.name)

    def add(self, other: pd.DataFrame, column: str) -> None:
        """ Sums the values from given column of another dataframe to its own

        :param other: pd.DataFrame: dataframe to add values from
        :param column: str: name of the column that should be added

        """
        self.time_series = pd.Series(
            other.loc[self.range, column] + self.time_series[self.name], name=self.name,
        )
