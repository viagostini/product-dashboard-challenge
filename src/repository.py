import os
from glob import glob

from src.product import Product


class ProductRepository:
    """ Manages a dictionary of available Products providing getter methods """

    def __init__(self, path: str) -> None:
        self.products = {}
        for product_path in glob(f"{path}/*"):
            if os.path.isdir(product_path):
                product = Product(product_path)
                self.products[product.name] = product

    def get_all(self) -> dict:
        """ Returns a dictionary with all available Products

        :returns: dictionary of Products
        """
        return self.products

    def get(self, product_name: str) -> Product:
        """ Returns a the Product with given name

        :param product_name: str: name of the requested Product
        :returns: Product named `product_name`

        """
        return self.products[product_name]
