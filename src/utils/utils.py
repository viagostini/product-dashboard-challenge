import os
import json
from typing import List, Tuple, Union

import pandas as pd
from datetime import date


def read_json(path: str) -> dict:
    """ Reads a json file into a Python dict

    :param path: str: path to the file
    :returns: dict with file content

    """
    with open(path) as file:
        return json.load(file)


def valid_scenario_files(path: str, extensions: List[str]) -> List[str]:
    """ Finds the paths of the valid scenario files in a directory

    :param path: str: path of the given directory
    :param extensions: List[str]: extensions that should be considered valid
    :returns: list of valid file paths

    """
    scenario_files = []
    for filename in os.listdir(path):
        _, extension = filename.split(".")
        if extension in extensions:
            scenario_files.append(f"{path}/{filename}")
    return scenario_files


def enforce_float_format(num: Union[str, float]) -> str:
    """ Cleans value coming from dataframe

    Note: if pandas was able to convert to float, input will be float and theres nothing
    to do. However, if its a string, we must clean it to be able to parse it

    :param num: Union[str, float]: float or str that should be numeric
    :returns: float if num is a float, cleaned str if num is str

    """
    if isinstance(num, str):
        return "".join(c for c in num if c.isdigit() or c in [".", "-"])
    return num


def one_year_projection_timestamps() -> Tuple[pd.Timestamp, pd.Timestamp]:
    """ Get pandas Timestamps for current date and one year in the future

    :returns: tuple of both current and next year's Timestamps
    """
    now = date.today()
    present = pd.Timestamp(year=now.year, month=now.month, day=1)
    future = pd.Timestamp(year=now.year + 1, month=now.month, day=1)
    return present, future
