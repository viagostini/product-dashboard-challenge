import numpy as np
import pandas as pd
from copy import deepcopy
from typing import List

from src.utils import utils


def average_growth(dataframe: pd.DataFrame, scenario: str, periods: int = 12) -> float:
    """ Calculates average growth of given scenario in windows of given size

    :param dataframe: pd.DataFrame: dataframe containing the scenario
    :param scenario: str: name of the scenario
    :param periods: int: period in months of the windows (Default value = 12)
    :returns: average growth in periods of given size

    """
    return dataframe[scenario].pct_change(periods=periods).mean() * 100


def projected_growth(
    dataframe: pd.DataFrame, scenario: str, initial: pd.Timestamp, end: pd.Timestamp
) -> float:
    """ Calculates projected growth of given scenario between two Timestamps

    :param dataframe: pd.DataFrame: dataframe containing the scenario
    :param scenario: str: name of the scenario
    :param initial: pd.Timestamp: initial timestamp
    :param end: pd.Timestamp: future timestamp
    :returns: projected growth between initial and end

    """
    curr = dataframe[scenario].loc[initial]
    next = dataframe[scenario].loc[end]
    return (next - curr) / curr * 100


def clean_numeric_column(df: pd.DataFrame, column: str) -> None:
    """ Cleans a dataframe column and parses it to numeric values inplace

    :param df: pd.DataFrame: dataframe containing the data
    :param column: str: name of the column to be cleaned

    """
    df[column] = df[column].replace("none", np.nan)
    df[column] = df[column].apply(utils.enforce_float_format)
    df[column] = pd.to_numeric(df[column], errors="coerce")


def interpolate_nans(df: pd.DataFrame, column: str) -> pd.DataFrame:
    """ Interpolates NaN values in dataframe column

    :param df: pd.DataFrame: dataframe containing the data
    :param column: str: name of the column to interpolate
    :returns: new column with NaN values interpolated

    """
    return df[column].interpolate(method="time", limit_direction="both")


def remove_outliers_from_dataframe(
    dataframe: pd.DataFrame, outlier_threshold: int = 3, exclude: List[str] = []
) -> pd.DataFrame:
    """ Replaces outliers from given dataframe with a new interpolated value

    The :param outlier_threshold: defines how many standard deviations above or below
    the mean is considered an outlier by this method

    :param dataframe: pd.DataFrame: dataframe to be modified
    :param outlier_threshold: int: threshold to be considered an outlier (Default = 3)
    :param exclude: List[str]: list of columns to ignore (Default value = [])
    :returns: new dataframe with outliers replaced with interpolated values

    """
    dataframe_copy = deepcopy(dataframe)
    for column in dataframe_copy:
        if column not in exclude:
            __interpolate_outliers(dataframe_copy, column, outlier_threshold)
    return dataframe_copy


def __interpolate_outliers(
    dataframe: pd.DataFrame, column: str, std_range: int = 4
) -> None:
    std = dataframe[column].std()
    mean = dataframe[column].mean()
    condition = (dataframe[column] - mean).abs() >= std_range * std
    dataframe.loc[condition, column] = np.NaN
    dataframe.loc[condition, column] = interpolate_nans(dataframe, column)
