from __future__ import annotations

from typing import List, Union

import pandas as pd

from src.dataframe_reader import DataFrameReader
from src.scenario import Scenario
from src.utils.utils import read_json, valid_scenario_files


class Product:
    """
    Represents a Product with some Scenarios

    During initialization reindexes all scenarios to match the union of the
    original indexes and sums the values from Base Scenario into all other scenarios
    """

    def __init__(self, path: str) -> None:
        config = read_json(f"{path}/config.json")
        self.name = config["name"]
        self.brand = config["brand"]
        self.scenarios = self.__parse_scenarios(path)

    def concatenate_scenarios(self, scenarios: Union[str, List[str]]) -> pd.DataFrame:
        """ Concatenates selected scenarios into a dataframe

        :param scenarios: Union[str, List[str]]: name of the scenarios to concatenate
        :returns: dataframe with concatenated scenarios

        """
        series = [self.scenarios[scenario].time_series for scenario in scenarios]
        dataframe = pd.concat(series, axis=1)
        return dataframe

    def __parse_scenarios(self, path: str) -> dict:
        scenarios = {}
        for file_path in valid_scenario_files(path, extensions=DataFrameReader.readers):
            scenario = Scenario(file_path=file_path)
            scenarios[scenario.name] = scenario
        self.__match_scenario_indexes(scenarios)
        self.__sum_base(scenarios)
        return scenarios

    def __match_scenario_indexes(self, scenarios: dict) -> None:
        proj_range = self.__projection_range(scenarios)
        for name, scenario in scenarios.items():
            if name != "Base Scenario":
                scenario.reindex(proj_range)

    def __projection_range(self, scenarios: dict) -> pd.DatetimeIndex:
        date_ranges = []
        for name, scenario in scenarios.items():
            if name != "Base Scenario":
                date_range = pd.date_range(
                    start=scenario.range[0], end=scenario.range[-1], freq="MS",
                )
                date_ranges.append(date_range)
        date_range = date_ranges[0].union_many(date_ranges[1:])
        return date_range

    def __sum_base(self, scenarios: dict) -> None:
        for name, scenario in scenarios.items():
            if name != "Base Scenario":
                scenario.add(scenarios["Base Scenario"].time_series, "Base Scenario")
