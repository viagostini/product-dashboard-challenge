import pandas as pd
import pytest
from pandas.testing import assert_index_equal, assert_series_equal

from src.dataframe_reader import DataFrameReader


@pytest.fixture
def product():
    product = {}
    product["values"] = [31.04, 30.89, 105.20]
    product["dates"] = ["2020-01-01", "2020-02-01", "2020-03-01"]
    product["path"] = "./tests/assets/test_product_1"
    return product


def test_read_csv(product):
    path = f"{product['path']}/base_scenario.csv"
    reader = DataFrameReader(extension="csv")

    data = reader.read(path, names=["Date", "Base Scenario"], index_col="Date")
    assert isinstance(data, pd.DataFrame)

    index = pd.DatetimeIndex(product["dates"], name="Date")
    expected = pd.Series(product["values"], name="Base Scenario", index=index)

    assert_series_equal(data["Base Scenario"], expected, check_exact=True)
    assert_index_equal(data.index, expected.index, check_exact=True)


def test_read_excel(product):
    path = f"{product['path']}/best_case_scenario.xlsx"
    reader = DataFrameReader(extension="xlsx")

    data = reader.read(path, names=["Date", "Best Case Scenario"], index_col="Date")
    assert isinstance(data, pd.DataFrame)

    index = pd.DatetimeIndex(product["dates"], name="Date")
    expected = pd.Series(product["values"], name="Best Case Scenario", index=index)

    assert_series_equal(data["Best Case Scenario"], expected, check_exact=True)
    assert_index_equal(data.index, expected.index, check_exact=True)
