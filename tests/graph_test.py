import pandas as pd
import plotly.graph_objects as go
import pytest

from src.graph import Graph
from src.product import Product


@pytest.fixture
def long_data():
    return pd.read_csv(
        "./tests/assets/long_scenario.csv",
        header=None,
        names=["Date", "Long Scenario"],
        index_col="Date",
        parse_dates=True,
    )


@pytest.fixture
def test_product():
    return Product("./tests/assets/test_product_1")


def test_graph_init(long_data):
    graph = Graph(long_data)
    assert graph.scenarios == ["Long Scenario"]


def test_graph_figure(long_data):
    graph = Graph(long_data)
    assert isinstance(graph.figure(), go.Figure)


def test_graph_metrics_content(long_data):
    graph = Graph(long_data)
    content = graph.metrics_content()
    assert "Projected growth" in content[0]
    assert "Average annual growth" in content[1]


def test_graph_metrics_content_empty(long_data):
    long_data = pd.concat([long_data, long_data], axis=1)
    graph = Graph(long_data)
    content = graph.metrics_content()
    assert content[0] == ""
    assert content[1] == ""


def test_graph_metrics_style(test_product):
    scenarios = ["Base Scenario"]
    dataframe = test_product.concatenate_scenarios(scenarios)
    graph = Graph(dataframe)
    assert graph.metrics_style() == ({}, {})


def test_graph_metrics_style_hide(test_product):
    scenarios = ["Worst Case Scenario", "Base Scenario"]
    dataframe = test_product.concatenate_scenarios(scenarios)
    graph = Graph(dataframe)
    assert graph.metrics_style() == ({"opacity": 0}, {"opacity": 0})
