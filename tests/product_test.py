import pandas as pd
from pandas.testing import assert_index_equal

from src.product import Product


def test_product():
    product = Product("./tests/assets/test_product_1")
    assert product.name == "Test Product A"
    assert product.brand == "Test Brand 1"
    assert isinstance(product.scenarios, dict)
    assert len(product.scenarios) == 3

    product = Product("./tests/assets/test_product_2")
    assert product.name == "Test Product B"
    assert product.brand == "Test Brand 2"
    assert isinstance(product.scenarios, dict)
    assert len(product.scenarios) == 3


def test_scenarios_dataframe():
    product = Product("./tests/assets/test_product_1")

    dataframe = product.concatenate_scenarios(
        scenarios=["Best Case Scenario", "Worst Case Scenario"]
    )

    assert isinstance(dataframe, pd.DataFrame)
    assert_index_equal(
        dataframe.columns, pd.Index(["Best Case Scenario", "Worst Case Scenario"])
    )
