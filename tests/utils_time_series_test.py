import pandas as pd
import pytest
from pandas.testing import assert_frame_equal

from src.utils.time_series import (
    average_growth,
    projected_growth,
    remove_outliers_from_dataframe,
)


def test_annual_average_growth():
    dataframe = pd.read_csv(
        "./tests/assets/long_scenario.csv",
        header=None,
        names=["Date", "Long Scenario"],
        index_col="Date",
        parse_dates=True,
    )
    actual = average_growth(dataframe, "Long Scenario", periods=12)
    assert pytest.approx(actual, 0.01) == 233.33


def test_projected_growth():
    dataframe = pd.read_csv(
        "./tests/assets/long_scenario.csv",
        header=None,
        names=["Date", "Long Scenario"],
        index_col="Date",
        parse_dates=True,
    )
    present = pd.Timestamp(year=2020, month=8, day=1)
    future = pd.Timestamp(year=2021, month=8, day=1)

    actual = projected_growth(dataframe, "Long Scenario", present, future)
    assert pytest.approx(actual, 0.01) == 400


def test_remove_outliers_from_dataframe():
    values = [31.04, 105.20, 30.89]
    new_values = [31.04, 30.9625, 30.89]
    index = pd.DatetimeIndex(["2020-01-01", "2020-02-01", "2020-03-01"])
    dataframe = pd.DataFrame(values, index=index)

    dataframe = remove_outliers_from_dataframe(dataframe, outlier_threshold=1)
    expected = pd.DataFrame(new_values, index=index)

    assert_frame_equal(dataframe, expected)
