import pandas as pd
import pytest
from pandas.testing import assert_frame_equal, assert_index_equal

from src.scenario import Scenario


@pytest.fixture
def test_scenario():
    test_scenario = {}
    test_scenario["values"] = [-12.04, -23.89, -8.20]
    test_scenario["dates"] = ["2020-01-01", "2020-02-01", "2020-03-01"]
    test_scenario["path"] = "./tests/assets/test_product_1/worst_case_scenario.csv"
    return test_scenario


def test_scenario_init(test_scenario):
    scenario = Scenario(file_path=test_scenario["path"])
    assert scenario.name == "Worst Case Scenario"
    assert isinstance(scenario.time_series, pd.DataFrame)

    index = pd.DatetimeIndex(test_scenario["dates"], name="Date")
    expected = pd.DataFrame(
        test_scenario["values"], columns=["Worst Case Scenario"], index=index
    )

    assert_frame_equal(scenario.time_series, expected, check_exact=True)
    assert_index_equal(scenario.time_series.index, expected.index, check_exact=True)
