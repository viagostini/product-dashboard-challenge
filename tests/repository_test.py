from src.product import Product
from src.repository import ProductRepository


def test_repository():
    repository = ProductRepository("./tests/assets")
    assert isinstance(repository.products, dict)
    assert len(repository.products) == 2


def test_repository_get_all():
    products = ProductRepository("./tests/assets").get_all()
    assert isinstance(products, dict)
    assert len(products) == 2
    assert set(products.keys()) == set(["Test Product A", "Test Product B"])
    for _, product in products.items():
        assert isinstance(product, Product)


def test_repository_get_single():
    product_A = ProductRepository("./tests/assets").get("Test Product A")
    assert isinstance(product_A, Product)
    assert product_A.name == "Test Product A"
