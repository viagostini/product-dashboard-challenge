from src.utils import utils


def test_projection_timestamps():
    present, future = utils.one_year_projection_timestamps()
    assert int((future - present).total_seconds()) == 365 * 24 * 3600


def test_valid_scenario_files():
    path = "./tests/assets/test_product_1"
    files = set(utils.valid_scenario_files(path, extensions=["csv", "xlsx"]))
    expected = set(
        [
            f"{path}/base_scenario.csv",
            f"{path}/best_case_scenario.xlsx",
            f"{path}/worst_case_scenario.csv",
        ]
    )
    assert files == expected


def test_enforce_float_format():
    assert utils.enforce_float_format(14.214) == 14.214
    assert utils.enforce_float_format("-14.214") == "-14.214"
    assert utils.enforce_float_format("14.214'") == "14.214"
    assert utils.enforce_float_format("xx14.214yy") == "14.214"
