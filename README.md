[![pipeline status](https://gitlab.com/viagostini/product-dashboard-challenge/badges/master/pipeline.svg)](https://gitlab.com/viagostini/product-dashboard-challenge/commits/master)
[![coverage report](https://gitlab.com/viagostini/product-dashboard-challenge/badges/master/coverage.svg)](https://gitlab.com/viagostini/product-dashboard-challenge/-/commits/master)



# Product Dashboard

<div align='center'><img src="images/screenshot.png" width=450 height=350></div>

## Overview
On startup the application loads the data contained in the given `data` folder and
the user is then free to view the different scenarios for each product. Multiple
scenarios of the same product can be viewed simultaneously to ease comparisons.

If a **single scenario** is selected, the dashboard will display two metrics related to
it: the average annual growth and the projected growth one year into the future.

The dashboard also allows the user to remove outliers from the graph by specifying
the maximum amount of standard deviations that a value can be away from the mean;
any point above that treshold will be replaced with a new interpolated value.

Finally, the user is able to download the currently viewed graph as an Excel
spreadsheet, including its current modifications such as outlier removal.

#### GitLab CI Pipeline
This project uses GitLab's own continuous integration/delivery tool to run some
style checks against the code and also run the test suite and automatically deploy
to Heroku, as showed in the following diagram.

![](images/pipeline.png)

In the fisrt stage, `black` is responsible for most of the style and
formatting checks and `flake8` does a little extra linting, such as unused imports,
for example. In the testing stage, the test suite is ran with `pytest` and finally,
if all the previous steps passed, the code is pushed to Heroku for deployment.

## How to run the application
### Heroku
This application is available as Heroku app at https://product-dashboard.herokuapp.com/

### Docker
This application is also available through docker, running the following commands:
`docker build -t product-dashboard .` and `docker run -p 8060:8060 product-dashboard`.

Then, the application should be available at `http://0.0.0.0:8060/`.

### Local

**Create virtual environment and install dependencies**

Use your favorite Python package manager to create a new environment with the dependencies listed in `requirements.txt`. For example, using `conda` you would use the command `conda create -n <env_name> --file requirements.txt` followed by `conda activate <env_name>`.

**Start the app**

With the environment active, just run `python app.py` and the application will be available at `http://0.0.0.0:8060/`.

## How to run the tests
Assuming a working environment with all the dependencies installed as described above, just run `pytest`.